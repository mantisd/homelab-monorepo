
## Validate
```sh
packer build  -var-file=credentials.pkr.hcl container-server/container-server.pkr.hcl
```

## Build
```sh
packer build  -var-file=credentials.pkr.hcl container-server/container-server.pkr.hcl
```

For cloud init to work the http/ folder needs to have both user-dat and meta-data files otherwise it wont work