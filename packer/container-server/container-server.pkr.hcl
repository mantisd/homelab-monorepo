
# Variable Definitions
variable "proxmox_api_url" {
  type = string
}

variable "proxmox_api_token_id" {
  type = string
}

variable "proxmox_api_token_secret" {
  type      = string
  sensitive = true
}


source "proxmox" "container-server" {

  # * Proxmox Info
  node        = "proxmox"
  proxmox_url = var.proxmox_api_url
  username    = var.proxmox_api_token_id
  token       = var.proxmox_api_token_secret
  # ? (Optional) Skip TLS Verification
  insecure_skip_tls_verify = true

  # * VM Info
  vm_name              = "container-server"
  vm_id                = "600"
  template_description = "Ubuntu Server jammy Image preconfigured with docker"
  iso_file             = "local:iso/ubuntu-22.04-live-server-amd64.iso"
  iso_storage_pool     = "local"
  unmount_iso          = true
  qemu_agent           = true
  scsi_controller      = "virtio-scsi-pci"
  
  disks {
    disk_size         = "25G"
    format            = "raw"
    storage_pool      = "local-lvm"
    storage_pool_type = "lvm"
    type              = "virtio"
  }

  # * VM CPU Settings
  cores = "4"

  # * VM Memory Settings
  memory = "4096" # 4GB RAM

  # * VM Network Settings
  network_adapters {
    model    = "virtio"
    bridge   = "vmbr1"
    firewall = "false"
  }

  # * VM Cloud-Init Settings
  cloud_init              = true
  cloud_init_storage_pool = "local-lvm"


  # * PACKER Boot Commands
  boot_command = [
    "<esc><wait>",
    "e<wait>",
    "<down><down><down><end>",
    "<bs><bs><bs><bs><wait>",
    "autoinstall ds=nocloud-net\\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ---<wait>",
    "<f10><wait>"
  ]
  boot      = "c"
  boot_wait = "5s"



  # PACKER Autoinstall Settings
  http_directory = "./container-server/http"
  # (Optional) Bind IP Address and Port
  http_bind_address = "10.10.1.88"
  http_port_min     = 8802
  http_port_max     = 8802

  ssh_username = "mantis"
  # (Option 1) Add your Password here
  ssh_password = "P@ssword"
  # - or -
  # (Option 2) Add your Private SSH KEY file here
  ssh_private_key_file = "~/.ssh/id_ed25519"

  # Raise the timeout, when installation takes longer
  ssh_timeout = "20m"
}

build {
  name    = "container-server"
  sources = ["source.proxmox.container-server"]

  # * Provisioning the VM Template for Cloud-Init Integration in Proxmox #1
  provisioner "shell" {
    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
      "sudo rm /etc/ssh/ssh_host_*",
      "sudo truncate -s 0 /etc/machine-id",
      "sudo apt -y autoremove --purge",
      "sudo apt -y clean",
      "sudo apt -y autoclean",
      "sudo cloud-init clean",
      "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
      "sudo sync"
    ]
  }

  # * Provisioning the VM Template for Cloud-Init Integration in Proxmox #2
  provisioner "file" {
    # * The source is relative to the directory where the packer command is being ran from
    source      = "./container-server/files/99-pve.cfg"
    destination = "/tmp/99-pve.cfg"
  }

  # * Provisioning the VM Template for Cloud-Init Integration in Proxmox #3
  provisioner "shell" {
    inline = ["sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg"]
  }

  # * Add additional provisioning scripts here
  provisioner "shell" {
    inline = [
      "sudo apt-get install -y curl ca-certificates gnupg lsb-release",
      "sudo apt-get update -y",
      "sudo curl -sSL https://get.docker.com | bash",
      "sudo usermod -aG docker $(whoami)"
    ]
  }

}
