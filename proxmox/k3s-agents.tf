variable "k3s_agent_password" {
  type      = string
  sensitive = true
  default   = ""
}

variable "k3s_agent_user" {
  type      = string
  sensitive = true
  default   = ""
}

#resource "proxmox_vm_qemu" "agent_1" {
#  vmid        = "401"
#  name        = "agent-1"
#  target_node = "proxmox"
#  desc        = "K3s - agent"
#  os_type     = "cloud-init"
#  ciuser      = var.k3s_agent_user
#  cipassword  = var.k3s_agent_password
#  clone       = "k3s-agent"
#  onboot      = true
#  agent       = 1
#  cores       = 4
#  sockets     = 1
#  cpu         = "host"
#  memory      = 4096
#  balloon     = 1024
#  network {
#    bridge = "vmbr1"
#    model  = "virtio"
#    tag    = -1
#  }
#  ipconfig0  = "ip=10.10.1.161/24,gw=10.10.1.1"
#  nameserver = "10.10.1.160"
#}