variable "portainer_password" {
  type      = string
  sensitive = true
  default   = ""
}

variable "portainer_user" {
  type      = string
  sensitive = true
  default   = ""
}
resource "proxmox_vm_qemu" "portainer" {
  vmid        = "601"
  name        = "portainer"
  target_node = "proxmox"
  desc        = "Container Host for containers on portainer"
  os_type     = "cloud-init"
  ciuser      = var.portainer_user
  cipassword  = var.portainer_password
  clone       = "container-server"
  onboot      = true
  agent       = 1
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 4096
  balloon     = 1024
  network {
    bridge = "vmbr1"
    model  = "virtio"
    tag    = -1
  }
  ipconfig0  = "ip=10.10.1.160/24,gw=10.10.1.1"
  nameserver = "1.1.1.1"
}

