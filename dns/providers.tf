variable "PIHOLE_PASSWORD" {
  type      = string
  sensitive = true
}

provider "pihole" {
  url      = local.pihole_primary_url # PIHOLE_URL
  password = var.PIHOLE_PASSWORD      # PIHOLE_PASSWORD
}


module "pihole_1" {
  source      = "./modules/pihole-records"
  domain_name = local.domain_name
  domain_ip   = local.domain_ip
  providers = {
    pihole = pihole
  }
}


