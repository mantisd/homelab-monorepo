
resource "pihole_dns_record" "demo_core_mantisd_io" {
  domain = var.domain_name
  ip     = var.domain_ip
}

