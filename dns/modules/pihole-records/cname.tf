resource "pihole_cname_record" "portainer" {
  domain = "portainer.${var.domain_name}"
  target = var.domain_name
}

resource "pihole_cname_record" "traefik" {
  domain = "traefik.${var.domain_name}"
  target = var.domain_name
}

resource "pihole_cname_record" "pihole" {
  domain = "pihole.${var.domain_name}"
  target = var.domain_name
}